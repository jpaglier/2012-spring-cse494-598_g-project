#!/usr/bin/python

'''
Created on Apr 11, 2012

@author: Justin
'''

#useful imports
import sys #used to return proper error codes
import os  #used to check file size
#http://docs.python.org/library/argparse.html?highlight=argparse#argparse
import argparse
import hashlib #for hashing

#fun imports
import subprocess #purely for the train easter egg

#Variables defining some of the --help info
CONST_PROGNAME = "analimage" #Yes, I went there kekekekekekekekekkee
CONST_DESCRIPTION = "A tool to automate analysis of raw disk image"

#Relevant offsets for extracting MBR from image
CONST_MBR_OFFSET = 0x0
CONST_MBR_LENGTH = 512
CONST_MBR_SIGNATURE = 0x55aa
CONST_MBR_SIGNATURE_OFFSET = 0x01fe
CONST_NUM_PRIMARY_PARTITIONS = 4

#Relevant offsets for extracting partition table from MBR
CONST_PART_TABLE_OFFSET = 0x01be
CONST_PART_TABLE_LENGTH = 64

#Length of an entry in the parition table
CONST_PART_RECORD_LENGTH = 16

#Relevant linux errorno's
CONST_ENOENT = 2 #no such file
CONST_EINVAL = 22 #invalid argument (image file)

#Constants relevant to partition record analysis
CONST_PART_TYPE_OFFSET = 0x04
CONST_PART_DICT =     {"\x00" : "Empty",
                       "\x01" : "DOS 12-bit FAT",
                       "\x04" : "DOS 16-bit FAT <32MB",
                       "\x05" : "Extended partition",
                       "\x06" : "DOS 16-bit FAT >32MB",
                       "\x07" : "NTFS", \
                       "\x08" : "AIX bootable partition",
                       "\x09" : "AIX data partition",
                       "\x0b" : "DOS 32-bit FAT",
                       "\x0c" : "DOS 32-bit FAT for interrupt 13 support",
                       "\x17" : "Hidden NTFS partition (XP and earlier)",
                       "\x1b" : "Hidden FAT32 partition",
                       "\x1e" : "Hidden VFAT partition",
                       "\x3c" : "Partition Magic recovery partition",
                       "\x66" : "Novell partitions",
                       "\x67" : "Novell partitions",
                       "\x68" : "Novell partitions",
                       "\x69" : "Novell partitions",
                       "\x81" : "Linux",
                       "\x82" : "Linux swap partition (or Solaris)",
                       "\x83" : "Linux native file system (extX Reiser, xiafs)",
                       "\x86" : "FAT16 volume/stripe set",
                       "\x87" : "HPFS fault-tolerant mirrored parttion or" + 
                                " NTFS volume/stripe set",
                       "\xa5" : "FreeBSD and BSD/386",
                       "\xa6" : "NetBSD",
                       "\xa9" : "OpenBSD",
                       "\xc7" : "Typical of corrupted NTFS volume/stripe set",
                       "\xeb" : "BeOS"}
CONST_FAT_PARTITIONS = ["\x04",
                        "\x06",
                        "\x0b",
                        "\x0c",
                        "\x1b"
                        ]
CONST_PART_SIZE_OFFSET = 0x0C
CONST_PART_SIZE_LENGTH = 4
CONST_START_CHS_HIGH_MASK = 0b11000000 #unused
CONST_START_CHS_LOW_MASK = 0b00111111 #unused
CONST_DIST_FROM_MBR_BEGIN = 0x08
CONST_DIST_FROM_MBR_LENGTH = 4

#HDD geometry constants - p standard
CONST_HEADS_PER_CYLINDER = 16
CONST_SECTORS_PER_TRACK = 63
CONST_SECTOR_SIZE = 512

#FAT VBR info
CONST_FAT_VBR_SIZE = 512
CONST_FAT_RES_START = 14
CONST_FAT_RES_LENGTH = 2
CONST_SECTORS_PER_CLUSTER_OFFSET = 13
CONST_NUM_FATS_OFFSET = 16
CONST_SIZE_OF_FAT16_BEGIN = 22
CONST_SIZE_OF_FAT16_LENGTH = 2
CONST_SIZE_OF_FAT32_BEGIN = 36
CONST_SIZE_OF_FAT32_LENGTH = 4
CONST_MAX_IN_ROOT16_BEGIN = 17
CONST_MAX_IN_ROOT16_LENGTH = 2
CONST_DIR_ENTRY_SIZE = 32

def setup_args():
    '''
    Set up argument and help message
    '''
    parser = argparse.ArgumentParser(CONST_PROGNAME, 
                                     description=CONST_DESCRIPTION)
    parser.add_argument("PATH", help="Path to raw disk image")
    
    return parser

def hash_file(path, file_data):
    '''
    Hashes the file_data and extracts the name from path
    Then writes the hashes to MD5-*.txt and SHA1-*.txt
    '''
    
    #open digest objects and feed them the data
    md5hasher = hashlib.md5(file_data)
    sha1hasher = hashlib.sha1(file_data)

    #extract the file name from the path
    if sys.platform == 'win32':
        dir_end = path.rfind('\\') + 1
    else: #assume we're running on a *nix
        dir_end = path.rfind('/') + 1
    
    file_name = path[dir_end:]
    
    #write the md5 hash to the file
    md5out_name = "MD5-{0}.txt".format(file_name)
    md5out = open(md5out_name, 'w')
    md5out.write(md5hasher.hexdigest())
    md5out.close()
    
    #write the sha1 hash to the file
    sha1out_name = "SHA1-{0}.txt".format(file_name)
    sha1out = open(sha1out_name, 'w')
    sha1out.write(sha1hasher.hexdigest())
    sha1out.close()
    
def get_mbr(file_data):
    '''
    Extract MBR from a raw disk image
    '''
    
    mbr = file_data[CONST_MBR_OFFSET:CONST_MBR_OFFSET + CONST_MBR_LENGTH]
    
    #print mbr.encode("hex") #debug line
    
    if mbr[CONST_MBR_SIGNATURE_OFFSET:].encode("hex") != '55aa':
        print 'Invalid MBR! Signature (0x55aa) missing! Check for tampering!'
        #sys.exit(CONST_EINVAL) #rem for forensic purposes - analyst can decide
    
    return mbr
    
def get_partition_table(mbr):
    '''
    Extracts the partition table from a raw disk image
    '''
    
    part_table = mbr[CONST_PART_TABLE_OFFSET:
                           CONST_PART_TABLE_OFFSET + CONST_PART_TABLE_LENGTH]
    
    #print part_table.encode("hex") #debug line
    
    return part_table

def process_partition_table(part_table):
    '''
    Loops through the partition table and
    passes each record off to the next method
    '''
    
    fat_results = list()
    
    #replace this loop with using the offsets - faster without mult
    for record_offset_multiple in range(4):
        record_offset = record_offset_multiple * CONST_PART_RECORD_LENGTH
        record = part_table[record_offset:
                            record_offset + CONST_PART_RECORD_LENGTH]
        
        #print record.encode("hex") #debug line
        
        #retrieve partition info
        part_info = process_partition_record(record, record_offset_multiple)

        #if its a fat partition store the info to return
        if part_info['part_type'] in CONST_FAT_PARTITIONS:
            fat_results.append(part_info)
            
    return fat_results
            
def get_fat_vbr(part_info, file_data):
    '''
    Grab the fat vbr based on the start sector
    '''
    
    #grab byte offset of start sector    
    start_byte = part_info['start_sector'] * CONST_SECTOR_SIZE
    
    end_byte = start_byte + CONST_FAT_VBR_SIZE
    fat_vbr = file_data[start_byte:end_byte] #grab the VBR

    return fat_vbr
            
def process_fat_vbr(part_info, fat_vbr):
    '''
    Process vbr in a fat partition:
        Reserved space
        Sectors/cluster
        FAT Area
        # of fats
        Size of each fat
        1st sector of cluster 2
    '''
    
    reserved_sectors_size = fat_vbr[CONST_FAT_RES_START:CONST_FAT_RES_START + \
                                     CONST_FAT_RES_LENGTH] #grab
    reserved_sectors_size = reserved_sectors_size[::-1] #reverse it (endian)
    
    reserved_sectors_size = int(reserved_sectors_size.encode("hex"), 16) #int
    
    #subtract 1 to prevent off-by-one error
    reserved_sectors_end = part_info['start_sector'] + reserved_sectors_size - 1
    
    reserved_output = "Reserved area: Start sector: {0} Ending sector:" + \
                         "{1} Size: {2} sectors"
    
    print reserved_output.format(part_info['start_sector'], 
                                 reserved_sectors_end, reserved_sectors_size)
    
    #grab and cast
    sectors_per_cluster = fat_vbr[CONST_SECTORS_PER_CLUSTER_OFFSET]
    sectors_per_cluster = int(sectors_per_cluster.encode("hex"), 16)
    print "Sectors per cluster: %d sectors" % sectors_per_cluster
    
    #get info about the FATs
    fat_start_sector = reserved_sectors_end + 1
    
    num_fats = int(fat_vbr[CONST_NUM_FATS_OFFSET].encode("hex"), 16)
    
    #grab the size of fat - if its fat32 this will be 0 and we'll look elsewhere
    
    size_of_fat = fat_vbr[CONST_SIZE_OF_FAT16_BEGIN:\
                          CONST_SIZE_OF_FAT16_BEGIN+CONST_SIZE_OF_FAT16_LENGTH]
    
    if size_of_fat == '\x00\x00': #fat32
        is_fat32 = True
        size_of_fat = fat_vbr[CONST_SIZE_OF_FAT32_BEGIN:\
                              CONST_SIZE_OF_FAT32_BEGIN+\
                              CONST_SIZE_OF_FAT32_LENGTH]
    else:
        is_fat32 = False

    #grab, reverse, encode
    size_of_fat = int(size_of_fat[::-1].encode("hex"), 16)
    
    #calculate the end
    fat_ending_sector = fat_start_sector + (size_of_fat * num_fats) - 1
    
    #give some output
    print "FAT Area: Start sector: %d Ending sector: %d" % \
                    (fat_start_sector, fat_ending_sector)
    print "# of FATs: %d" % num_fats
    print "The size of each FAT: %d sectors" % size_of_fat
    
    if is_fat32: #data area starts right after FAT area
        cluster2_begin = fat_ending_sector + 1
    else: #fat16 - data area starts after root dir
        max_files_in_root = fat_vbr[CONST_MAX_IN_ROOT16_BEGIN: \
                                    CONST_MAX_IN_ROOT16_BEGIN + \
                                    CONST_MAX_IN_ROOT16_LENGTH]
        max_files_in_root = int(max_files_in_root[::-1].encode("hex"), 16)
        root_dir_size = \
            (max_files_in_root * CONST_DIR_ENTRY_SIZE) / CONST_SECTOR_SIZE
        cluster2_begin = fat_ending_sector + root_dir_size + 1
        
    print "The first sector of cluster 2: %d" % cluster2_begin

def process_partition_record(part_record, part_number):
    partition_type_number = part_record[CONST_PART_TYPE_OFFSET]
    partition_type = CONST_PART_DICT[partition_type_number]
    
    '''
    start_head = ord(part_record[1])
    
    cylsec1 = ord(part_record[2]) #grab and convert to int
    cylsec2 = ord(part_record[3]) #grab and convert to int
    
    
    extracted_bits = cylsec1 & CONST_START_CHS_HIGH_MASK #extract high 2 bits
    start_cylinder = extracted_bits << 2 #place the hi 2 bits (&shft out)
    start_cylinder = start_cylinder | cylsec2 #grab low 8 bits
    
    #print ((0 |  (ord(cylsec1) & 0b11000000)) << 2) | ord(cylsec2) #debug
    
    start_physical_sector = cylsec1 & CONST_START_CHS_LOW_MASK #relevant bits
    
    #debug
    print "C:{0}".format(start_cylinder)
    print "H:{0}".format(start_head)
    print "S:{0}".format(start_physical_sector)
    
    print type(start_cylinder)
    print type(start_head)
    print type(start_physical_sector)
       
    start_lba_sector = chs_to_lba(start_cylinder, start_head,
                                  start_physical_sector)
    '''
    
    #grab distance from MBR (start sector)
    start_sector = part_record[CONST_DIST_FROM_MBR_BEGIN:\
                               CONST_DIST_FROM_MBR_BEGIN + \
                               CONST_DIST_FROM_MBR_LENGTH]
    #account for endianness (reverse) and convert to int
    start_sector = int(start_sector[::-1].encode("hex"), 16)

    #read in bytes - LITTLE ENDIAN
    num_sectors = part_record[CONST_PART_SIZE_OFFSET:
                              CONST_PART_SIZE_OFFSET + CONST_PART_SIZE_LENGTH]
    num_sectors = num_sectors[::-1] #reverse it to make it normal
    num_sectors = int(num_sectors.encode("hex"), 16) #convert to int

    print "({0}) {1}, {2}, {3}".format(partition_type_number.encode("hex"), 
                                      partition_type, start_sector,
                                      num_sectors)

    return {'partition_number':part_number, 
            'part_type':partition_type_number, 
            'start_sector':start_sector,
            'num_sectors':num_sectors}

def chs_to_lba(cylinder, head, sector):
    '''
    Converts CHS to LBA
    
    CHS(0,0,1) = LBA(0) kk bai
    '''
    
    #check that chs value makes sense, output warning if it doesn't
    if cylinder < 0 or head < 0 or head >= CONST_HEADS_PER_CYLINDER or \
       sector < 1 or sector > CONST_SECTORS_PER_TRACK:
        print "WARNING: Invalid CHS value (%d, %d, %d)! Translating anyway..." \
                % (cylinder, head, sector) 
    
    lba= (((cylinder * CONST_HEADS_PER_CYLINDER) + head)
           * CONST_SECTORS_PER_TRACK) + sector - 1 
    
    return lba

def main():
    '''
    Main method for the program
    '''
    
    #retrieve arguments
    args = setup_args().parse_args() 
    
    try:
        #easter egg
        if args.PATH == 'train':
            try:
                subprocess.call(['sl', "-aF"])
                print "CHOO CHOO!!"
            
            except:
                print "If you don't know 'bout sl you best ax somebody!"
            
        elif os.path.getsize(args.PATH) < CONST_MBR_LENGTH \
           and not "/dev" in args.PATH:
            print "%s is too small to be an MBR or disk image!" % args.PATH
            sys.exit(CONST_EINVAL)
        else: #normal execution starts here
            #open the file, read the data, close the file
            with open(args.PATH, 'rb') as image_file: #READ ONLY
                file_data = image_file.read()
            
            #hash the data
            hash_file(args.PATH, file_data)
            
            #extract mbr (little superflous 
            #for the purposes of the assignment so we may optimize out) and
            #signal for garbage collection comment out if we remove mbr grab
            mbr = get_mbr(file_data)
                        
            #grab partition table
            part_table = get_partition_table(mbr)
            #uncomment next two lines if we remove the mbr extraction
            #part_table = get_partition_table(file_data)
            #file_data = None
            
            
            #print nice little heading
            print #newline
            print "Partition Table Results".center(79)
            print '=' * 79
            
            #retrieve info about any FAT fs
            fat_parts_info = process_partition_table(part_table)
            #print part_table.encode("hex") #debug line
            
            #print nice little table
            print #newline
            print "Volume Boot Record Results".center(79)
            print '=' * 79
            
            #loop through any FAT file systems that exist
            for x in fat_parts_info:
                #notify user FAT was found
                print "FAT file system found in partition " + \
                        str(x['partition_number'])
                fat_vbr = get_fat_vbr(x, file_data) #grab its VBR
                process_fat_vbr(x, fat_vbr) #process it
                print #newline
                
    except IOError: #exit with proper ERRNO if there's a problem
        print "File not found or is a directory!"
        sys.exit(CONST_ENOENT)
    except OSError:
        print "File not found or is a directory!"
        sys.exit(CONST_ENOENT)

if __name__ == '__main__':
    '''
    Entry point
    '''
    main()