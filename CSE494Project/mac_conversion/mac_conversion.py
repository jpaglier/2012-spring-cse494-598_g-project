#!/usr/bin/python

'''
Created on Apr 15, 2012

@author: justin
'''

import argparse
import sys
import datetime

#program information

CONST_PROGNAME = 'mac_conversion'
CONST_DESCRIPTION = 'Convert mac hex data into a date or time. For multiple' + \
                    ' inputted values, consider only the first hex value.'

#Relevant linux errorno's
CONST_ENOENT = 2 #no such file
CONST_EINVAL = 22 #invalid argument (image file)

#Time bitmasks
CONST_HOUR_MASK   =  0b1111100000000000
CONST_MINUTE_MASK =  0b0000011111100000
CONST_SECOND_MASK =  0b0000000000011111

#time shifting constant
CONST_HOUR_RSHIFT = 11
CONST_MINUTE_RSHIFT = 5
CONST_SECOND_LSHIFT = 1 #need to mult by 2

#Date bitmasks
CONST_YEAR_MASK  = 0b1111111000000000
CONST_MONTH_MASK = 0b0000000111100000
CONST_DAY_MASK   = 0b0000000000011111
CONST_MIN_YEAR_OFFSET   = 0
CONST_MAX_YEAR_OFFSET   = 127

#date shifting constanst
CONST_YEAR_RSHIFT = 9
CONST_MONTH_RSHIFT = 5

#Date constants
CONST_BASE_YEAR = 1980

def setup_args():
    '''
    Set up argument and help message
    '''
    parser = argparse.ArgumentParser(CONST_PROGNAME, 
                                     description=CONST_DESCRIPTION)
    
    output_options = parser.add_argument_group(title='Output options')
    output_me_options = output_options.add_mutually_exclusive_group( \
                                                                required=True)
    output_me_options.add_argument('-T', '--time', 
                                   help='Use time conversion module. Either' + \
                                        ' -f or -h must be given.', 
                                   action='store_true')
    output_me_options.add_argument('-D', '--date', 
                                   help='Use date conversion module. Either' + \
                                        ' -f or -h must be given.', 
                                   action='store_true')
    
    input_options = parser.add_argument_group(title='Input options')
    input_me_options = input_options.add_mutually_exclusive_group(required=True)
    input_me_options.add_argument('-f', '--file', 
                                  help="This specifies the path to a file" + \
                                  " that includes a hex value of time or" + \
                                  " date. Note that the hex value should" + \
                                  " follow this notation: 0x1234.",
                                  dest="filename")
    input_me_options.add_argument('-x', '--hex', 
                                  help="This specifies a hex value for" + \
                                  " converting into either a date or time" + \
                                  " value. Note that the hex should follow" + \
                                  " this notation: 0x1234.",
                                  dest="hex_value")
    
    return parser
    
def convert_to_date(hex_value):
    '''
    Convert 16 bit hex value into date
    '''
    from datetime import date
    
    #extract values
    year = hex_value & CONST_YEAR_MASK #grab the year bits
    year = year >> CONST_YEAR_RSHIFT #shift to the right to get the proper value
    year = decode_year(year)
    
    month = hex_value & CONST_MONTH_MASK
    month = month >> CONST_MONTH_RSHIFT
    
    day = hex_value & CONST_DAY_MASK
    
    try:
        date = date(year, month, day)
    except ValueError as ve:
        print ve
        sys.exit(CONST_EINVAL)
    
    return "Date: " + date.strftime("%B %d, %Y")

def decode_year(year_offset):
    if year_offset >= CONST_MIN_YEAR_OFFSET or \
       year_offset <= CONST_MAX_YEAR_OFFSET:
        return 1980 + year_offset

def convert_to_time(hex_value):
    '''
    Convert 16 bit hex value into time
    '''
    
    from datetime import time
    
    #extract values
    hour = hex_value & CONST_HOUR_MASK
    hour = hour >> CONST_HOUR_RSHIFT
    
    minute = hex_value & CONST_MINUTE_MASK
    minute = minute >> CONST_MINUTE_RSHIFT
    
    second = hex_value & CONST_SECOND_MASK
    second = second << CONST_SECOND_LSHIFT #multiply by 2 because of encoding
    
    try:
        time = time(hour, minute, second)
    except ValueError as ve:
        print ve
        sys.exit(CONST_EINVAL)
    
    
    return "Time: " + time.strftime("%I:%M:%S %p")
    #return "Time: %02d:%02d:%02d %s" % (hour,minute,second, am_pm)
    
def main():
    args = setup_args().parse_args()
    
    #grab value from wherever is relevant
    if args.filename != None:
        fin = open(args.filename, 'r')
        hex_value = fin.readline()
        fin.close()
    else:
        hex_value = args.hex_value
            
    #check to make sure this is actually hex input
    if hex_value[0:2] != '0x' and len(hex_value) != 6:
        print "Illegal input! Hex value should be in the format \"0xABCD\""
        sys.exit(CONST_EINVAL)
        
    hex_value = int(hex_value, 16) #convert to int
            
    if args.time:
        result = convert_to_time(hex_value)
    else: #args.date
        result = convert_to_date(hex_value)

    print result
    
if __name__ == '__main__':
    main()