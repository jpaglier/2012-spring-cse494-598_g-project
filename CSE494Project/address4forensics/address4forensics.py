#!/usr/bin/python

'''
Created on Apr 11, 2012

@author: Justin
'''

import argparse #http://docs.python.org/library/argparse.html?highlight=argparse#argparse
import sys

'''
gooby pls
'''

CONST_PROG_NAME = "address4forensics"
CONST_DESCRIPTION = """Convert between three different address types when an address of a 
different type is given."""

def setup_args():
    parser = argparse.ArgumentParser(CONST_PROG_NAME, description=CONST_DESCRIPTION)
    format_group = parser.add_argument_group(title='Result Type')
    format_me_group = format_group.add_mutually_exclusive_group(required=True)
    format_me_group.add_argument('-L', '--logical', 
                                 help="Calculate the logical address from either the cluster address or the physical address. Either -c or -p must be given.", 
                                 action='store_true')
    format_me_group.add_argument('-P', '--physical', 
                                 help="Calculate the physical address from either the cluster address or the logical address. Either -c or -l must be given.", 
                                 action='store_true')
    format_me_group.add_argument('-C', '--cluster', 
                                 help="Calculate the cluster address from either the logical address or the physical address. Either -l or -p must be given.", 
                                 action='store_true')
    
    byte_address_group = parser.add_argument_group(title="Byte Value Options")
    byte_address_group.add_argument('-B', '--byte-address', 
                                    help="Instead of returning sector values for the conversion, this returns the byte address of the calculated value, which is the number of sectors multiplied by the number of bytes per sector.",
                                    action='store_true')
    '''
    Ideally we'd add -s to a subgroup but it doesn't seem to work
    '''
    #TODO: see if we can fix this
    sector_size_group = byte_address_group.add_argument_group()
    byte_address_group.add_argument('-s', '--sector-size',
                                    type=int,
                                    help="When the -B option is used, this allows for a specification of bytes per sector other than the default 512. Has no effect on output without -B.",
                                    dest="sector_size",
                                    default=512)
    
    input_options_group = parser.add_argument_group(title="Input Options")
    input_options_me_group = input_options_group.add_mutually_exclusive_group(required=True)
    input_options_me_group.add_argument("-l", "--logical-known",
                                        type=int,
                                        help="This specifies the known logical address for calculating either a cluster address or a physical address. When used with the -L option, this simply returns the value given for address.")
    input_options_me_group.add_argument("-p", '--physical-known',
                                        type=int,
                                        help="This specifies the known physical address for calculating either a cluster address or a logical address. When used with the -P option, this simply returns the value given for address.")
    input_options_me_group.add_argument('-c', '--cluster-known',
                                        type=int,
                                        help="This specifies the known cluster address for calculating either a logical address or a physical address. When used with the -C option, this simply returns the value given for address. Note that options -k, -r, -t, and -f must be provided with this option.")
    
    partition_options_group = parser.add_argument_group(title="Partition Options")
    partition_options_group.add_argument('-b', '--partition-start',
                                         type=int,
                                         help="This specifies the physical address (sector number) of the start of the partition, and defaults to 0 for ease in working with images of a single partition. The offset value will always translate into logical address 0.",
                                         dest="offset",
                                         default=0)
    partition_options_group.add_argument('-k', '--cluster-size',
                                         type=int,
                                         help="This specifies the number of sectors per cluster")
    partition_options_group.add_argument("-r", '--reserved',
                                         type=int,
                                         help="This specifies the number of reserved sectors in the partition")
    partition_options_group.add_argument("-t", '--tables',
                                         type=int,
                                         help="This specifies the number of FAT tables, which is usually 2",
                                         default=2)
    partition_options_group.add_argument('-f', '--fat-length',
                                         type=int,
                                         help="This specifies the length of each FAT table in sectors",)
    
    
    return parser

def caution():
    print "ERROR: Input type is same as output. Did you really mean to do that?"

def logical(args):
    #Doin' whatever to output a logical address
    
    if args.logical_known != None:
        caution()
        #return args.logical_known    #hurr
            
    elif args.physical_known != None:
        return args.physical_known - args.offset
        
    elif args.cluster_known != None:
        if args.cluster_size == None  or\
           args.reserved == None      or\
           args.tables == None or\
           args.fat_length == None:
            print "Use of -c requires -k, -r, and -f. -t is optional"\
                     + " and defaults to 2"
            sys.exit()
        return ((args.cluster_known - 2) * args.cluster_size) +\
             (args.fat_length * args.tables) + args.reserved

def physical(args):
    if args.physical_known != None:
        caution()
        #return args.physical_known    #hurr
            
    elif args.cluster_known != None:
        if args.cluster_size == None  or\
           args.reserved == None      or\
           args.tables == None or\
           args.fat_length == None:
            print "Use of -c requires -k, -r, and -f. -t is optional"\
                     + " and defaults to 2"
            sys.exit()
        return ((args.cluster_known - 2) * args.cluster_size) +\
            (args.fat_length * args.tables) + args.offset + args.reserved
        
    elif args.logical_known != None:
        return args.logical_known + args.offset

def cluster(args):
    if args.cluster_size == None  or\
        args.reserved == None      or\
        args.tables == None or\
        args.fat_length == None:
            print "Use of -C requires -k, -r, and -f. -t is optional"\
                     + " and defaults to 2"
            sys.exit()
    if args.cluster_known != None:
        caution()
        #return args.cluster_known    #hurr
                
    elif args.logical_known != None:
        cluster = (args.logical_known - args.reserved - 
                     (args.tables * args.fat_length)) / \
                     (args.cluster_size * args.sector_size) + 2
        if cluster < 2:
            print "That is before the clusters start!"
            sys.exit()
        else:  
            return cluster
        
    elif args.physical_known != None:
        cluster = (args.physical_known - args.reserved - 
                   (args.tables * args.fat_length) - args.offset) / \
                   (args.cluster_size * args.sector_size) + 2
        if cluster < 2:
            print "That is before the clusters start!"
            sys.exit()
        else:  
            return cluster
            
def main():
    args = setup_args().parse_args()
    
    print args
    
    if args.logical:
        result = logical(args)
    
    elif args.physical:
        result = physical(args)
    
    elif args.cluster:
        result = cluster(args)
        
    else:
        print "no output type specified!"
        return
    
    if args.byte_address:
        result = result*args.sector_size    #add in final byte conversion if requested
    
    print result
    

if __name__ == '__main__':
    main()
